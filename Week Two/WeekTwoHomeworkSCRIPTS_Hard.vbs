

Sub GetTicker():

' 1st subroutine creates and populates the table with yearly change,
' yearly change percentage, and yearly volume (easy and moderate on the homework)

 ' set up the structure of the destination table

Cells(1, 10).Value = "Ticker"
Cells(1, 11).Value = "Yearly Change"
Cells(1, 12).Value = "Percent Change"
Cells(1, 13).Value = "Yearly Volume"

' set structure of the "hard" table
'headers
Cells(1, 17).Value = "Ticker"
Cells(1, 18).Value = "Value"

' rows
Cells(2, 16).Value = "Greatest % Increase"
Cells(3, 16).Value = "Greatest % Decrease"
Cells(4, 16).Value = "Greatest Total Volume"


Dim i As Long ' i is column A
Dim j As Long ' Is column J
Dim ActiveRows As Long

ActiveRows = Cells(Rows.Count, 1).End(xlUp).Row


j = 1 ' is the row of the unique ticker in J



For i = 2 To ActiveRows


' if new ticker add to column J and set volume

If Cells(i, 1).Value <> Cells(i - 1, 1).Value Then
    j = j + 1
    Dim RunningVolume As Double
    RunningVolume = Cells(i, 7).Value
    
    Dim OpenPrice As Double
    OpenPrice = Cells(i, 3).Value
    
   Cells(j, 10).Value = Cells(i, 1).Value ' set ticker

   Cells(j, 13).Value = RunningVolume
   
 ' If same ticker just update the running total
   
 Else:

    RunningVolume = RunningVolume + Cells(i, 7).Value
    Cells(j, 13).Value = RunningVolume
    
    Dim ClosePrice As Double
    ClosePrice = Cells(i, 6).Value
    
    Dim YearlyChange As Double
    YearlyChange = (ClosePrice - OpenPrice)
    
    Cells(j, 11).Value = YearlyChange
    
    ' color the cell red or green
    
    If YearlyChange > 0 Then
        Cells(j, 11).Interior.ColorIndex = 4 ' green
        ElseIf YearlyChange < 0 Then
            Cells(j, 11).Interior.ColorIndex = 3 ' red
     End If
     
    
    Dim PercentChange As Double
    
    ' Don't divide by zero!
        If OpenPrice <> 0 Then
            PercentChange = (YearlyChange / OpenPrice)
            Cells(j, 12).Value = PercentChange
            Cells(j, 12).NumberFormat = "0.00%"
        Else
            PercentChange = 0
            Cells(j, 12).Value = PercentChange
            Cells(j, 12).NumberFormat = "0.00%"
         End If
    

End If

Next i

End Sub

Sub LastTable():

' Fill the last table from yearly summary table

' This fills greatest percent increase, decrease, and volume from
' previous table. Column J (10)

Dim Increase

Increase = Cells(2, 12).Value

Dim Decrease

Decrease = Cells(2, 12).Value

Dim Volume

Volume = Cells(2, 13).Value

Dim HighTicker As String
HighTicker = Cells(2, 10).Value

Dim LowTicker As String
LowTicker = Cells(2, 10).Value

Dim VolumeTicker As String
VolumeTicker = Cells(2, 10).Value

' preset with first ticker values

Cells(2, 17).Value = HighTicker

Cells(3, 17).Value = LowTicker

Cells(4, 17).Value = VolumeTicker

' preset values of the tickers

Cells(2, 18).Value = Increase
Cells(2, 18).NumberFormat = "0.00%"

Cells(3, 18).Value = Decrease
Cells(3, 18).NumberFormat = "0.00%"

Cells(4, 18).Value = Volume



For i = 2 To Cells(Rows.Count, 10).End(xlUp).Row

    If Cells(i, 12).Value > Increase Then
    
        HighTicker = Cells(i, 10).Value ' update ticker
        Cells(2, 17).Value = HighTicker
        
        Increase = Cells(i, 12).Value ' update increase
        Cells(2, 18).Value = Increase
        
        
        
     ElseIf (Cells(i, 12).Value < Decrease) Then
     
        LowTicker = Cells(i, 10).Value ' update ticker
        Cells(3, 17).Value = LowTicker
        
        Decrease = Cells(i, 12).Value  ' update decrease
        Cells(3, 18).Value = Decrease
        
        
    End If
    
    If Cells(i, 13) > Volume Then
        VolumeTicker = Cells(i, 10).Value ' update ticker
        Cells(4, 17).Value = VolumeTicker
        
        Volume = Cells(i, 13).Value ' update volume
        Cells(4, 18).Value = Volume
    End If

Next i

End Sub






